<div class="panel panel-default">
  <div class="panel-body">
     <ul class="list-group">
       <li class="list-group-item">
           <a href="{{ url('/admin/employees') }}"> <i class="fa fa-btn fa-users"> Employee</i> </a>
       </li>
       <li class="list-group-item">
           <a href="{{ url('/admin/leave') }}"> <i class="fa fa-btn fa-legal"> Leave</i> </a>
       </li>
     </ul>
  </div>
</div>
