@extends('layouts.backend',['name'  => 'leaves'])

@section('content')
    <div class="header">
      <a href="{{ url('admin/leaves') }}" class="btn btn-primary"><i class="fa fa-reply"></i> Back</a> 
    </div>
    <hr>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Employee</th><th>Leave Day</th><th>Leave Type</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $leave->id }}</td> <td> {{ $leave->employee }} </td><td> {{ $leave->leave_day }} </td><td> {{ $leave->leave_type }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection