@extends('layouts.backend',['name'  => 'leave'])

@section('content')
    <div class="header">
      <a href="{{ url('admin/leave/create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> New leave</a> 
    </div>
    <hr>
    <div class="table">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>S.No</th><th>Employee</th><th>Leave Day</th><th>Leave Type</th><th>Actions</th>
                </tr>
            </thead>
            <tbody>
            {{-- */$x=0;/* --}}
            @foreach($leave as $item)
                {{-- */$x++;/* --}}
                <tr>
                    <td>{{ $x }}</td>
                    <td><a href="{{ url('admin/leave', $item->id) }}">{{ $item->employee }}</a></td><td>{{ $item->leave_day }}</td><td>{{ $item->leave_type }}</td>
                    <td>
                        <a href="{{ url('admin/leave/' . $item->id . '/edit') }}">
                            <button type="submit" class="btn btn-primary btn-xs">
                            <i class="fa fa-edit"></i>
                            Edit</button>
                        </a> /
                        @include('admin.partials.delete', array('data' => $item, 'name' => 'leave' ))
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="pagination"> {!! $leave->render() !!} </div>
    </div>

@endsection
