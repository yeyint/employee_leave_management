@extends('layouts.backend',['name'  => 'leave'])

@section('content')
    <div class="header">
      <a href="{{ url('admin/leave') }}" class="btn btn-primary"><i class="fa fa-reply"></i> Back</a> 
    </div>
    <hr>
    {!! Form::open(['url' => 'admin/leave', 'class' => 'form-horizontal']) !!}

                <div class="form-group {{ $errors->has('employee') ? 'has-error' : ''}}">
                {!! Form::label('employee', 'Employee: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('employee', $employees,null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('employee', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('leave_day') ? 'has-error' : ''}}">
                {!! Form::label('leave_day', 'Leave Day: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('leave_day', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('leave_day', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('leave_type') ? 'has-error' : ''}}">
                {!! Form::label('leave_type', 'Leave Type: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('leave_type',$leave_types ,null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('leave_type', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('remark') ? 'has-error' : ''}}">
                {!! Form::label('remark', 'Remark: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('remark', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('remark', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

    <hr>
    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit('Create', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

@endsection