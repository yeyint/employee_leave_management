@extends('layouts.backend',['name'  => 'employees'])

@section('content')
    <div class="header">
      <a href="{{ url('admin/employees/create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> New employees</a> 
    </div>
    <hr>
    <div class="table">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>S.No</th><th>Name</th><th>Email</th><th>Age</th><th>Actions</th>
                </tr>
            </thead>
            <tbody>
            {{-- */$x=0;/* --}}
            @foreach($employees as $item)
                {{-- */$x++;/* --}}
                <tr>
                    <td>{{ $x }}</td>
                    <td><a href="{{ url('admin/employees', $item->id) }}">{{ $item->name }}</a></td><td>{{ $item->email }}</td><td>{{ $item->age }}</td>
                    <td>
                        <a href="{{ url('admin/employees/' . $item->id . '/edit') }}">
                            <button type="submit" class="btn btn-primary btn-xs">
                            <i class="fa fa-edit"></i>
                            Edit</button>
                        </a> /
                        @include('admin.partials.delete', array('data' => $item, 'name' => 'employees' ))
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="pagination"> {!! $employees->render() !!} </div>
    </div>

@endsection
