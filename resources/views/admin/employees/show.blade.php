@extends('layouts.backend',['name'  => 'employees'])

@section('content')
    <div class="header">
      <a href="{{ url('admin/employees') }}" class="btn btn-primary"><i class="fa fa-reply"></i> Back</a> 
    </div>
    <hr>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Name</th><th>Email</th><th>Age</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $employee->id }}</td> <td> {{ $employee->name }} </td><td> {{ $employee->email }} </td><td> {{ $employee->age }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection