## Employee Leave Management

Requirements
============
-require internet for iconfonts


Installation
=========
```sh
git clone  

composer install

```

Migration
=========
```sh
php artisan migrate
php artisan db:seed
```

Admin
======
```sh
user - admin@example.com
pass - rootroot
```

