<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
            Schema::create('employees', function(Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->text('email');
                $table->integer('age');
                $table->date('start_date');
                $table->integer('leave_day')->default(14);
                $table->timestamps();
            });
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employees');
    }

}
