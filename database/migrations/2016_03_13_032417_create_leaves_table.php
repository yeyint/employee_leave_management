<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLeavesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
            Schema::create('leaves', function(Blueprint $table) {
                $table->increments('id');
                $table->string('employee');
                $table->integer('leave_day');
                $table->string('leave_type');
                $table->text('remark');
                $table->integer('is_approve')->default(0);
                $table->timestamps();
            });
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('leaves');
    }

}
