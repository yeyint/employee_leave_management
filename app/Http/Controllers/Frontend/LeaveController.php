<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Leave;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class LeaveController extends Controller
{   

    protected $model;

    function __construct(Leave $leave)
    {
        $this->model = $leave;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $leave = $this->model->with('employee')->paginate(15);
        return view('admin.leave.index', compact('leave'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {   
        $employees = $this->model->getEmployeeList();
        return view('admin.leave.create', compact('employees'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['employee' => 'required', 'leave_day' => 'required', 'leave_type' => 'required', ]);

        $this->model->create($request->all());

        Session::flash('flash_message', 'Leave added!');

        return redirect('admin/leave');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $leave = $this->model->findOrFail($id);

        return view('admin.leave.show', compact('leave'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $leave = $this->model->findOrFail($id);
        $employees = $this->model->getEmployeeList();
        return view('admin.leave.edit', compact('leave','employees'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['employee' => 'required', 'leave_day' => 'required', 'leave_type' => 'required', ]);

        $leave = $this->model->findOrFail($id);
        $leave->update($request->all());

        Session::flash('flash_message', 'Leave updated!');

        return redirect('admin/leave');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $this->model->destroy($id);

        Session::flash('flash_message', 'Leave deleted!');

        return redirect('admin/leave');
    }

}
