<?php


  /*
  |--------------------------------------------------------------------------
  | Frontend Routes
  |--------------------------------------------------------------------------
  |
  | Here is the routes group for frontend
  |
  |
  */
 

 Route::group(['prefix' => '/', 'namespace' => 'Frontend'], function ($router) {
     
    $router->get('/', function () {

        return view('home');
    });
   

 });



/*
|--------------------------------------------------------------------------
| Backend Routes
|--------------------------------------------------------------------------
|
| Here is where you can add routes for backed
|
|
*/


Route::get(config('backend.url', 'backend'), function () {
   
    return redirect()->route('admin.dashboard');

});

Route::group([
          'namespace'    => 'Admin',
          'middleware'   => ['auth','admin'],
          'prefix'       => 'admin', 
          ],
          function () {

          Route::get('/dashboard', ['as' => 'admin.dashboard', function () {
              return view('admin.dashboard');
          }]);

          Route::resource('employees', 'EmployeesController');
          Route::resource('leave', 'LeaveController');

          
        

});




/*
|--------------------------------------------------------------------------
| Authentication routes...
|--------------------------------------------------------------------------
|
| Here is the routes for auth
|
|
*/

Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');
  

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');
