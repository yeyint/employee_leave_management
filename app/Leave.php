<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leave extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'leaves';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['employee', 'leave_day', 'leave_type','remark'];

    

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'employee');
    }


    public function getEmployeeList()
    {
        return Employee::lists('name', 'id');
    }


    public function getLeaveTypes()
    {
        $leave_types = [ 'medical_leave'    => 'Medical Leave',
                         'childcare_leave'  => 'Childcare Leave',
                         'maternity_leave'  => 'Maternity Leave',
                         'sick_leave'       => 'Sick Leave',
                       ];
                       
        return $leave_types;
    }
}
